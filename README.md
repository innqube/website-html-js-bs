# [Kleer Website](http://www.kleer.la/) 

[![InnQUbe](http://www.innqube.com/powered-by-innqube.png)](http://www.innqube.com/)

Este proyecto es la nueva versión del sitio web corporativo de la empresa Kleer (http://www.kleer.la).

## ¿Cómo comenzar?

Para descargar y ejecutar el proyecto en local hay que clonar el repositorio:   `git clone https://bitbucket.org/innqube/website-html-js-bs.git`

Existe una [demo](http://www.innqube.com/kleer/) en el servidor de InnQube.


## Errores y comentarios

Si encontraste algún bug o un tienes comentario puedes enviarnos un email a [hola@innqube.com](mailto:hola@innqube.com) o puedes [abrir una nueva issue](https://bitbucket.org/innqube/website-html-js-bs/issues/new) en el tracker de este proyecto. .

## Colaboradores

Colaboran en este proyecto

### Funcionales / POs

* Coti Molinari - Kleer ([coti@kleer.la](coti@kleer.la))
* Martín Salías - Kleer ([martin.salias@kleer.la](martin.salias@kleer.la))
* Pablo Tortorella - Kleer ([pablo.tortorella@kleer.la](pablo.tortorella@kleer.la))


### Consultoría de diseño

* Martín Gorrichio ([mg@gorricho.com.ar](mg@gorricho.com.ar))

### Diseño y desarrollo

Mauro Strione - InnQube ([mauro.strione@innqube.com](mauro.strione@innqube.com))

## Copyright

Copyright (C) 2016 Kleer Latinoamérica